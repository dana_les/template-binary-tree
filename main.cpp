﻿#include <iostream>
#include <string>

using namespace std;

template <class T>class BSNode
{
private:
	T _data;
	BSNode<T>* _left;
	BSNode<T>* _right;

public:
	BSNode(T data)
	{
		{
			_data = data;
			_left = nullptr;
			_right = nullptr;
		}
	}
	BSNode(const BSNode<T>& other)
	{
		_data = other->data;
		_left = other->_left;
		_right = other->_right;
	}
	~BSNode()
	{
		deleteTree(this);
	}
	BSNode<T>& operator=(const BSNode<T>& other)
	{
		other->data = _data;
		other->_left = _left;
		other->_right = _right;
		return(*this);
	}

	void deleteTree(BSNode<T> n)
	{
		if (n)
		{
			deleteTree(n->Left());
			deleteTree(n->Right());
			delete this;
		}
	}
	BSNode<T> copyTree(BSNode<T> n)
	{
		if (other == NULL)
		{
			return NULL;
		}

		BSNode<T>* newNode = new BSNode<T>();
		newNode->_data = other->_data;
		newNode->left = copyTree(other->left);
		newNode->right = copyTree(other->right);
		return newNode;
	}
	bool isLeaf() // מחזירה האם הצומת הנוכחי הוא עלה
	{
		if ((!_left) && (!_right))
		{
			return(true);
		}
		else
		{
			return(false);
		}
	}
	void insertNode(BSNode<T>* newNode)// מוסיפה לפי הכלל: קטן/שווה משמאל, גדול מימין
	{
		if (newNode->_data > _data)
		{

			if (_right != nullptr)
			{
				_right->insertNode(newNode);
			}
			else
			{
				_right = newNode;
			}
		}
		else if (newNode->_data <= _data)
		{
			if (_left != nullptr)
			{
				_left->insertNode(newNode);
			}
			else
			{
				_left = newNode;
			}
		}

	}
	BSNode<T>* search(T val)// מחפשת בעץ אחר הערך, אם נמצא תחזיר את הצומת, אם לא תחזיר NULL
	{

		if (val > _data)
		{
			if (_right != nullptr)
			{
				return(_right->search(val));

			}
			else
			{
				return(nullptr);
			}
		}
		else if (val < _data)
		{
			if (_left != nullptr)
			{
				return(_left->search(val));
			}
			else
			{
				return(nullptr);
			}
		}
		else
		{
			return(this);

		}
	}
};


template <class T>class BSTree
{
private:
	BSNode<T>* _root;
public:
	BSTree()
	{
		_root = nullptr;
	}
	BSTree(T rootVal)
	{
		_root = new BSNode<T>(rootVal);
	}
	BSTree(const BSTree<T>& other)
	{
		*this = other;
	}
	~BSTree()
	{
		this->~BSNode();
	}

	BSTree<T>& operator=(const BSTree<T>& other)
	{
		root = other._root;
		copyTree(_root);
	}

	void insertValue(T val)//יוצרת צומת עם הערך ומוסיפה אותו לעץ לפי הכלל
	{
		BSNode<T> *newNode = new BSNode<T>(val);
		_root->insertNode(newNode);
	}
	BSNode<T>* search(T val)// מחפשת בעץ אחר הערך, אם נמצא תחזיר את הצומת, אם לא תחזיר NULL
	{
		return(_root->search(val));
	}
};
//check
int main(int argc, char** argv)
{
	int x = 10;
	BSTree<int> *tree = new BSTree<int>(x);
	tree->insertValue(15);
	tree->insertValue(5);
	tree->insertValue(12);
	if ((tree->search(1)) != nullptr)
	{
	cout << "exist\n";
	}
	system("pause");

	return 0;

}
